import express from 'express';
import fs from 'fs';
import mongoose from 'mongoose';
import { emitter } from './emitter.js';
import { userRouter, adminRouter, fileWorker, roomRouter } from './routes/index.js';
import { login, authJWT } from './controllers/authController.js';

const urlencodedParser = express.urlencoded({ extended: true });
const app = express();

mongoose.connect("mongodb://localhost:27017/database", { useUnifiedTopology: true, useNewUrlParser: true }, function (err) {
    if (err) return console.log('Что-то пошло не так! :/');

    app.listen(3000, function () {
        console.log("Сервер работает.");
    });
});

app.post('/login', urlencodedParser, login);
app.use('/admin', urlencodedParser, adminRouter);
app.use('/user', urlencodedParser, userRouter);
app.use("/fileWorker", fileWorker);
app.use("/room", authJWT, urlencodedParser, roomRouter);
app.use("/", function (req, res) {
    res.send(`<h1>Главная страница</h1>`);
});

emitter.on("writeFile", function (fileName) {
    let readableStream = fs.createReadStream(fileName, "utf8");
    let writeableStream = fs.createWriteStream("newFile.txt");
    readableStream.pipe(writeableStream);
});