import { Admin } from "../models/adminModel.js";

export const createAdmin = async (req, res) => {
    try {
        let admin = await Admin.findOne({ nickname: req.body.nickname, name: req.body.name, password: req.body.password });

        if (admin) {
            return res.status(400).send('Администратор с таким именем уже существует.');
        } else {
            const newAdmin = await new Admin({ nickname: req.body.nickname, name: req.body.name, password: req.body.password });

            await newAdmin.save(function (err) {
                if (err) return res.status(400).send(`Error: ${err}`);
            });

            return res.send(`<h1>Регистрация нового администратора</h1>
                            <p>Никнейм: ${newAdmin.nickname}</p>
                            <p>Имя: ${newAdmin.name}</p>`);
        }
    } catch (e) {
        return res.status(500).send(`Error: ${e.message}`);
    }
};

export const getAdminById = async (req, res) => {
    try {
        let admin = await Admin.findOne({ _id: req.params.id });

        return admin ? res.send(`<p>Получаем администратора с id: ${admin.id}. \nНикнейм: ${admin.nickname}</p>`)
                    : res.status(400).send(`<h1>Администратора с таким id не существует.</h1>`);
    } catch (e) {
        return res.status(500).send(`Error: ${e.message}`);
    }
};

export const deleteAdminById = async (req, res) => {
    try {
        let admin = await Admin.findOne({ _id: req.params.id });

        if (admin) {
            Admin.findOneAndDelete({ _id: req.params.id }, function (err, result) {
                if (err) return res.status(404).send(`Error: ${err}`);
            });

            return res.send(`<p>Удалили администратора с id: ${admin.id}. \nНикнейм: ${admin.nickname} :(</p>`);
        } else {
            return res.status(400).send(`<h1>Администратора с таким id не существует.</h1>`);
        }
    } catch (e) {
        return res.status(500).send(`Error: ${e.message}`);
    }
};

export const updateAdminById = async (req, res) => {
    try {
        let admin = await Admin.findOne({ _id: req.params.id });

        if (admin) {
            Admin.updateOne({ _id: req.params.id }, { nickname: req.body.nickname }, { new: true }, function (err, result) {
                if (err) return res.status(404).send(`Error: ${err}`);
            });

            return res.send(`<p>Апдейт учётной записи администратора с id: ${admin.id}. \nНовый никнейм: ${req.body.nickname}</p>`);
        } else {
            return res.status(400).send(`<h1>Администратора с таким id не существует.</h1>`);
        }
    } catch (e) {
        res.status(500).send(`Error: ${e.message}`);
    }
};