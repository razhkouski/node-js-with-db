import jwt from 'jsonwebtoken';
import { User } from '../models/userModel.js';
import { Admin } from '../models/adminModel.js';

const accessTokenSecret = 'super*!9secret#&5token';

export const login = async (req, res) => {
    let user = await User.findOne({ name: req.body.name, password: req.body.password });
    if (user) {
        const accessToken = jwt.sign({ name: user.name, role: user.role, _id: user._id }, accessTokenSecret, { expiresIn: '24h' });
        return res.json({ accessToken });
    }    

    let admin = await Admin.findOne({ nickname: req.body.nickname, password: req.body.password });    
    if (admin) {
        const accessToken = jwt.sign({ nickname: admin.nickname, role: admin.role, _id: admin._id }, accessTokenSecret, { expiresIn: '24h' });
        return res.json({ accessToken });
    }
    
    return res.status(400).send('Не найдено пользователей с такой комбинацией логина и пароля, либо параметры не были переданы.');
};

export const authJWT = (req, res, next) => {
    try {
        const authHeader = req.headers.authorization;

        if (authHeader) {
            const token = authHeader.split(' ')[1];

            jwt.verify(token, accessTokenSecret, (err, user) => {
                if (err) {
                    return res.status(400).send("Неверный токен.");
                }
                req.user = user;
                next();
            });
        } else {
            return res.status(400).send("Не проведена авторизация.");
        }
    } catch (e) {
        return res.status(400).send("Неверные параметры запроса.");
    }
};

export const checkRole = (req, res, next) => {
    const { role } = req.user;

    if (role !== 'admin') return res.status(403).send('Недостаточно прав доступа.');
    next();
};