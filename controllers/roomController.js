import { Room } from "../models/roomModel.js";
import { Admin } from "../models/adminModel.js";
import { User } from "../models/userModel.js";

export const createRoom = async (req, res) => {
    try {
        let room = await Room.findOne({ number: req.body.number });

        if (room) {
            return res.status(400).send('Комната с таким номером уже существует.');
        } else {
            let newRoom = await new Room({ number: req.body.number, users: req.body.users }),
                roomAdmin = await Admin.findOne({ _id: req.user._id });

            newRoom.admin = req.user._id;
            await newRoom.save();
            
            roomAdmin.rooms.push(newRoom._id);
            await roomAdmin.save();

            if (Array.isArray(req.body.users)) {
                await User.updateMany({_id: req.body.users}, { $set: { rooms: newRoom._id } });
            } else {
                let user = await User.findOne({ users: req.body.user });

                user.rooms.push(newRoom._id);
                await user.save();
            }

            return res.send(`Создана комната №${newRoom.number}.`);
        }
    } catch (e) {
        return res.status(400).send('Неверные параметры запроса. Не удалось создать комнату.');
    }
};

export const deleteRoom = async (req, res) => {
    try {
        let room = await Room.findOne({ _id: req.params.id }),
            roomAdmin = await Admin.findOne({ _id: req.user._id });

        if (room && roomAdmin._id.toString() === room.admin._id.toString()) {
            Room.deleteOne({ _id: req.params.id }, function (err, result) {
                if (err) return res.status(400).send(err);
            });            
            Admin.updateOne({ _id: req.user._id }, { $pull: { rooms: room._id }}, { new: true }, function (err, result) {
                if (err) return res.status(400).send(err);
            });

            return res.send(`Удалена комната №${room.number}.`);
        } else {
            return res.status(400).send('Недостаточно прав или не удалось найти комнату.');
        }
    } catch (err) {
        return res.status(400).send('Неверные параметры запроса. Не удалось удалить комнату.');
    }
};

export const updateRoom = async (req, res) => {
    try {
        let room = await Room.findOne({ _id: req.params.id }),
            roomAdmin = await Admin.findOne({ _id: req.user._id });

        if (room && roomAdmin._id.toString() === room.admin._id.toString()) {
            Room.updateOne({ _id: req.params.id }, { number: req.body.number }, { new: true }, function (err, result) {
                if (err) return res.status(400).send(err);
            });
            await room.save();

            return res.send(`Апдейт комнаты ${room.number}`);
        } else {
            return res.status(400).send('Недостаточно прав.');
        }
    } catch (e) {
        res.status(400).send('Неверные параметры запроса. Не удалось апдейтнуть комнату.');
    }
};

export const getListOfRooms = async (req, res) => {
    try {
        if (req.user.role === 'admin') {
            let admin = await Admin.findOne({ _id: req.user._id });
            return res.send(`Ваш список комнат: ${admin.rooms}`);
        } else {
            let user = await User.findOne({ _id: req.user._id });
            return res.send(`Ваш список комнат: ${user.rooms}`);
        }
    } catch (e) {
        return res.status(400).send('Неверные параметры запроса. Не удалось получить список комнат.');
    }
};

export const getRoomById = async (req, res) => {
    try {
        let room = await Room.findOne({ _id: req.params.id });

        if (req.user.role === 'admin') {
            return res.send(`Нашли вашу комнату №${room.number}`);
        } else {
            for (let i = 0; i < room.users.length; i++) {
                if (room.users[i]._id.toString() == req.user._id.toString()) return res.send(`Нашли вашу комнату №${room.number}`);
            }
            return res.send('Недостаточно прав, либо данной комнаты не существует.');
        }
    } catch (e) {
        return res.status(400).send('Неверные параметры запроса. Не удалось получить комнату по указанному id.');
    }
};