import { User } from "../models/userModel.js";

export const createUser = async (req, res) => {    
    try {
        let user = await User.findOne({ name: req.body.name });

        if (user) {
            return res.status(400).send(`<h1>Пользователь с таким именем уже существует.</h1>`);
        } else {
            const newUser = new User({ name: req.body.name, surname: req.body.surname, password: req.body.password, age: req.body.age });

            await newUser.save(function (err) {
                if (err) return res.status(400).send(`Error: ${err}`);
            });

            return res.send(`<h1>Регистрация нового пользователя</h1>
                        <p>Имя: ${newUser.name}</p>
                        <p>Фамилия: ${newUser.surname}</p>
                        <p>Возраст: ${newUser.age}</p>`);
        }
    } catch (e) {
        return res.status(500).send(`Error: ${e.message}`);
    }
};

export const getUserById = async (req, res) => {
    try {
        let user = await User.findOne({ _id: req.params.id });
        return user ? res.send(`<p>Получаем пользователя с id: ${user.id}. \nИмя пользователя и фамилия: ${user.name} ${user.surname}</p>`)
                    : res.status(400).send(`<h1>Пользователя с таким id не существует.</h1>`);
    } catch (e) {
        return res.status(500).send(`Error: ${e.message}`);
    }
};

export const deleteUserById = async (req, res) => {
    try {
        let user = await User.findOne({ _id: req.params.id });

        if (user) {
            User.findOneAndDelete({ _id: req.params.id }, function (err, result) {
                if (err) return res.status(404).send(`Error: ${err}`);
            });

            return res.send(`<p>Удалили пользователя с id: ${user.id}. \nИмя пользователя и фамилия: ${user.name} ${user.surname} :(</p>`);
        } else {
            return res.status(400).send(`<h1>Пользователя с таким id не существует.</h1>`);
        }
    } catch (e) {
        return res.status(500).send(`Error: ${e.message}`);
    }
};

export const updateUserById = async (req, res) => {
    try {
        let user = await User.findOne({ _id: req.params.id });

        if (user) {
            User.updateOne({ _id: req.params.id }, { name: "Vova" }, { new: true }, function (err, result) {
                if (err) return res.status(404).send(`Error: ${err}`);
            });

            return res.send(`<p>Апдейт учётной записи пользователя с id: ${user.id}. \nИмя пользователя и фамилия: ${user.name} ${user.surname}</p>`);
        } else {
            return res.send(`<h1>Пользователя с таким id не существует.</h1>`);
        }
    } catch (e) {
        return res.status(500).send(`Error: ${e.message}`);
    }
};