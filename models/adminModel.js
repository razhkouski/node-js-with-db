import mongoose from 'mongoose';
import autopopulate from 'mongoose-autopopulate';

const adminSchema = new mongoose.Schema({
    nickname: { type: String, unique: true },
    name: { type: String, required: true },
    password: { type: String, required: true },
    role: { type: String, default: "admin" },
    rooms: [{ type: mongoose.Schema.Types.ObjectId, ref: "room", autopopulate: true }]
}, { versionKey: false });

adminSchema.plugin(autopopulate);
const Admin = mongoose.model('admin', adminSchema);

export { Admin, adminSchema };