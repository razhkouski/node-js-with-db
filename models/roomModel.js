import mongoose from 'mongoose';
import autopopulate from 'mongoose-autopopulate';

const roomSchema = new mongoose.Schema({
    number: { type: Number, required: true, unique: true },
    users: [{ type: mongoose.Schema.Types.ObjectId, ref: "user", autopopulate: true }],
    admin: { type: mongoose.Schema.Types.ObjectId, ref: "admin", autopopulate: true }
}, { versionKey: false });

roomSchema.plugin(autopopulate);
const Room = mongoose.model('room', roomSchema);

export { Room, roomSchema };
