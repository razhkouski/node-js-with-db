import mongoose from 'mongoose';
import autopopulate from 'mongoose-autopopulate';

const userSchema = new mongoose.Schema({
    name: { type: String, required: true },
    surname: { type: String, required: true },
    password: { type: String, required: true },
    age: { type: Number, default: 18 },
    rooms: [{ type: mongoose.Schema.Types.ObjectId, ref: "room", autopopulate: true }]
}, { versionKey: false });

userSchema.plugin(autopopulate);
const User = mongoose.model('user', userSchema);

export { User, userSchema };