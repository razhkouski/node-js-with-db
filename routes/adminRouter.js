import express from 'express';
import { createAdmin, getAdminById, deleteAdminById, updateAdminById } from '../controllers/adminController.js';

const adminRouter = express.Router();

adminRouter.post("/create", createAdmin);
adminRouter.get("/:id", getAdminById);
adminRouter.delete("/:id", deleteAdminById);
adminRouter.put("/:id", updateAdminById);

export default adminRouter;