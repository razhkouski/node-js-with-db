import express from 'express';
import multer from 'multer';
import { writeSimple, readSimple, writeStream, readStream, writeFile } from '../controllers/fileWorkerController.js';

const upload = multer();
const fileWorker = express.Router();

fileWorker.get("/writeSimple", writeSimple);
fileWorker.get("/readSimple", readSimple);
fileWorker.get("/writeStream", writeStream);
fileWorker.get("/readStream", readStream);
fileWorker.post("/writeFile",  upload.single('file'), writeFile);

export default fileWorker;