import userRouter from './userRouter.js';
import adminRouter from './adminRouter.js';
import fileWorker from './fileWorkerRouter.js';
import roomRouter from './roomRouter.js';

export { userRouter, adminRouter, fileWorker, roomRouter };