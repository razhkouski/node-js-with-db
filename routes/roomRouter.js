import express from 'express';
import { createRoom, deleteRoom, updateRoom, getListOfRooms, getRoomById } from '../controllers/roomController.js';
import { checkRole } from '../controllers/authController.js';

const roomRouter = express.Router();

roomRouter.post("/create", checkRole, createRoom);
roomRouter.delete("/:id", checkRole, deleteRoom);
roomRouter.put("/:id", checkRole, updateRoom);
roomRouter.get("/list", getListOfRooms);
roomRouter.get("/:id", getRoomById);

export default roomRouter;