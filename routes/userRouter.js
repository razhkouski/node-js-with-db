import express from 'express';
import { createUser, getUserById, deleteUserById, updateUserById } from '../controllers/userController.js';

const userRouter = express.Router();

userRouter.post("/create", createUser);
userRouter.get("/:id", getUserById);
userRouter.delete("/:id", deleteUserById);
userRouter.put("/:id", updateUserById);

export default userRouter;